# CertiPro

Sistema para gestionar certificados.

## Introducción ☘

CertiPro pretende ser un sistema de creación de certificados para entes
organizadores de eventos. Diseñado bajo un enfoque de autoservicio, intenta
ofrecer las opciones necesarias para diseñar plantillas de certificados,
especificar los tipos de eventos y otros aspectos necesarios para gestionar
certificados dentro de una organización.

CertiPro se encuentra actualmente en desarrollo, por lo que este readme ofrece
una visión general de este proceso.

### Prerequisitos

*  Python 3+
*  Django 3.0+
*  PostreSQL 10+

## Estilos y Convenciones 📑

Intento seguir las pautas dadas por Django y PEP 8 siempre y cuando mi sentido
común me diga lo contrario 😜.

### Sintaxis

*  *Clases*:

|               | Estilo      | Ejemplo      |
| ------------- | ----------- | ------------ |
| Identificador | PascalCase  | BarModel     |
| Propiedad     | snake_case  | make_foo_bar |
| Método        | snake_case  | make_foo_bar |

*  *Funciones independientes* (helpers):

|               | Estilo      | Ejemplo      |
| ------------- | ----------- | ------------ |
| Identificador | snake_case  | make_foo_bar |

*  *Variables*:

|               | Estilo     | Ejemplo      |
| ------------- | ---------- | ------------ |
| Identificador | snake_case | foo_bar      |

*  *Constantes*:

|               | Estilo      | Ejemplo      |
| ------------- | ----------- | ------------ |
| Identificador | SNAKE_CASE  | FOO_BAR      |

## Pruebas ⚙
Esto es una tarea pendiente, aún no he desarrollado las pruebas unitarias. Son
importantísimas, así que las habrá.

## Autor 🖋
2020 **Prof. Maikel Carballo**

## Licencia 🗝
MIT. Más información lea el archivo [LICENSE](LICENSE).

## Agradecimientos 🙏
Este proyecto es posible gracias a la existencia de otros
proyectos, por lo tanto, mi más sincero agradecimiento a todas las personas
que, de alguna u otra forma –creadores(as), autores(as), diseñadores(as),
programadores(as)...–, han hecho posible que exista Python, Django,
librerías/bibliotecas para Python y Django, PostgreSQL, computadoras, Linux...
y por encima de todo al Infinito Creador.