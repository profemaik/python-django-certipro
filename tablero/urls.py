from django.urls import path

from tablero import views

app_name = 'tablero'
urlpatterns = [
    path('', views.index, name='index'),
    path('reporte-general/', views.reporte, name='reporte-general'),
]
