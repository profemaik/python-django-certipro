from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from asistentes.models import Asistente



@login_required
def index(request):
    """Página de inicio del sitio web o proyecto.

    Arguments:
        request {object} -- Objeto HttpRequest

    Returns:
        object -- Objeto HttpResponse con plantilla y contexto renderizados.
    """
    minifichas_cuantificadoras = {
        'ficha_asistentes': {
            'clase_css': 'primary',
            'titulo': 'Asistentes registrados',
            'cantidad': Asistente.objects.count(),
            'icono': 'fas fa-users'
        },
    }
    context = {
        'site_title': 'CertiPro | Tablero',
        'page_title': 'Tablero',
        'minifichas': minifichas_cuantificadoras,
        'url_reporte': 'tablero:reporte-general',
        'url_admin': 'admin:index',
    }

    return render(request, 'tablero/index.html', context=context)


def reporte(request):
    """TODO definir esta vista para los reportes generales.

    Arguments:
        request {object} -- Objeto HttpResponse con plantilla y contexto renderizados.
    """
    pass
