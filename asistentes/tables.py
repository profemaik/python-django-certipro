import django_tables2 as tables

from .models import Asistente


class TablaAsistentes(tables.Table):
    """Datos tabulados de los asistentes registrados.

    Arguments:
        tables {class} -- django_tables2
    """
    numero_ci_humano = tables.Column(verbose_name='Número de CI')
    acciones = tables.TemplateColumn(template_name='asistentes/botones_acciones.html', orderable=False)

    class Meta:
        model = Asistente
        template_name = "django_tables2/bootstrap4.html"
        fields = ('numero_ci_humano', 'nacionalidad', 'nombres', 'apellidos',)
        sequence = ('numero_ci_humano', 'nacionalidad', 'nombres', 'apellidos',)
