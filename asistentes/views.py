from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin

from .filters import FiltroAsistente
from .models import Asistente
from .tables import TablaAsistentes


class VistaListaAsistentes(LoginRequiredMixin, SingleTableMixin, FilterView):
    """Renderiza el catálogo/listado de asistentes registrados en el sistema,
    en formato de tabla.

    Arguments:
        ListView {class} -- Vista de lista genérica de Django.
    """
    model = Asistente
    template_name = 'asistentes/lista-asistentes.html'
    table_class = TablaAsistentes
    paginate_by = 10
    filterset_class = FiltroAsistente

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site_title'] = 'CertiPro | Asistentes'
        context['page_title'] = 'Asistentes'
        context['content_title'] = 'Asistentes registrados'
        context['url_reporte'] = 'asistentes:reporte-general'
        context['url_admin'] = 'admin:asistentes_asistente_changelist'
        return context


class VistaAsistenteEnDetalle(LoginRequiredMixin, DetailView):
    """Renderiza el registro detallado de un asistente particular.

    Arguments:
        DetailView {class} -- Vista detallada genérica de Django.
    """
    model = Asistente
    template_name = 'asistentes/detalle-asistente.html'


class VistaReporteGeneralAsistentes(DetailView):
    pass
