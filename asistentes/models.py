from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.validators import MinValueValidator
from django.db import models


class Asistente(models.Model):
    """Almacena los datos de la persona que `ha participado en un evento`.

    Arguments:
        models (class) -- Modelo base de Django.

    Returns:
        string -- Maikel Carballo - V 12345678 (ejemplo).
    """

    NACIONALIDAD_V = 'V'
    NACIONALIDAD_E = 'E'
    NACIONALIDADES = [
        (NACIONALIDAD_V, 'Venezolana(o)'),
        (NACIONALIDAD_E, 'Extranjera(o)'),
    ]
    nacionalidad = models.CharField(max_length=1, choices=NACIONALIDADES, default=NACIONALIDAD_V)
    numero_ci = models.PositiveIntegerField('número de CI', unique=True, validators=[MinValueValidator(1),])
    nombres = models.CharField('nombre(s)', max_length=200, help_text='Ej.: Ana María')
    apellidos = models.CharField('apellido(s)', max_length=200, help_text='Ej.: Pérez Pérez')

    class Meta:
        verbose_name = 'Asistente'
        verbose_name_plural = 'Asistentes'
        ordering = ['nombres']

    def __str__(self):
        return f'{self.nombres} {self.apellidos} - {self.nacionalidad} {self.numero_ci_humano}'

    @property
    def numero_ci_humano(self):
        """Formatea el número de cédula de identidad con separador de miles
        según la configuración regional establecida.

        Returns:
            string -- Ejemplo: 99.999.999
        """
        return intcomma(self.numero_ci)

    def numero_ci_humano_admin(self):
        """Formatea el número de cédula de identidad con separador de miles
        según la configuración regional establecida, en el admin.

        Returns:
            string -- Ejemplo: 99.999.999
        """
        return intcomma(self.numero_ci)
    numero_ci_humano_admin.admin_order_field = 'numero_ci'
    numero_ci_humano_admin.short_description = 'número de CI'
