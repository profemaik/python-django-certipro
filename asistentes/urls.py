from django.urls import path

from .views import (VistaAsistenteEnDetalle, VistaListaAsistentes, VistaReporteGeneralAsistentes)

app_name = 'asistentes'
urlpatterns = [
    path('asistentes/', VistaListaAsistentes.as_view(), name='index'),
    path('asistente-en-detalle/<int:pk>', VistaAsistenteEnDetalle.as_view(), name='detalle'),
    path('reporte-general-asistentes/', VistaReporteGeneralAsistentes.as_view(), name='reporte-general'),
]
