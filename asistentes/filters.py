import django_filters
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db import models
from django.forms.widgets import NumberInput, TextInput

from .models import Asistente


class FiltroAsistente(django_filters.FilterSet):
    """Filtros para las consultas de los asistentes registrados.

    Arguments:
        FilterSet {class} -- django_filters
    """
    numero_ci = django_filters.NumberFilter(
        field_name='numero_ci',
        widget=NumberInput(attrs={'placeholder': 'Número de CI'}),
        lookup_expr='icontains',
    )
    nombres = django_filters.CharFilter(
        field_name='nombres',
        widget=TextInput(attrs={'placeholder': 'Nombres'}),
        lookup_expr='icontains',
    )
    apellidos = django_filters.CharFilter(
        field_name='apellidos',
        widget=TextInput(attrs={'placeholder': 'Apellidos'}),
        lookup_expr='icontains',
    )

    class Meta:
        model = Asistente
        fields = ['nacionalidad',]
