from django.contrib import admin

from .models import Asistente


@admin.register(Asistente)
class AsistenteAdmin(admin.ModelAdmin):
    """Administrador para los asistentes.

    Args:
        ModelAdmin (class): Modelo Admin base de Django.
    """
    list_display = ('numero_ci_humano_admin', 'nacionalidad', 'nombres', 'apellidos',)
    list_filter = ('nacionalidad',)
    search_fields = ('nombres', 'apellidos', 'numero_ci')
