import django_tables2 as tables

from .models import Tema


class TablaTemario(tables.Table):
    """Datos tabulados de los temas registrados.

    Arguments:
        Table {class} -- Representación de una tabla según django-tables2.
    """
    acciones = tables.TemplateColumn(template_name='temario/botones_acciones.html', orderable=False)

    class Meta:
        model = Tema
        template_name = "django_tables2/bootstrap4.html"
        fields = ('titulo',)
