from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin

from .filters import FiltroTemario
from .models import DetalleTema, Tema
from .tables import TablaTemario


class VistaListaTemario(LoginRequiredMixin, SingleTableMixin, FilterView):
    """Renderiza el catálogo/listado de temas registrados en el sistema,
    en formato de tabla.

    Args:
        LoginRequiredMixin (class): Verifica que el usuario esté autentificado.
        SingleTableMixin (class): Agrega una tabla HTMl al contexto.
        FilterView (class): Renderiza los filtros de búsqueda.

    Returns:
        string: HTML de la vista.
    """
    model = Tema
    template_name = 'temario/lista-temario.html'
    table_class = TablaTemario
    paginate_by = 10
    filterset_class = FiltroTemario

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site_title'] = 'CertiPro | Temario'
        context['page_title'] = 'Temario'
        context['content_title'] = 'Temas registrados'
        context['url_reporte'] = 'tablero:index'
        context['url_admin'] = 'admin:temario_tema_changelist'
        return context


class VistaTemarioEnDetalle(LoginRequiredMixin, DetailView):
    """Renderiza los detalles de un tema (ediciones, horas académicas y
    contenidos programáticos).

    Args:
        LoginRequiredMixin (class): Verifica que el usuario esté autentificado.
        DetailView (class): Vista genérica detallada de Django.
    """
    model = Tema
    template_name = 'temario/detalle-temario.html'
