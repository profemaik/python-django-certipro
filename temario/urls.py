from django.urls import path

from .views import VistaListaTemario, VistaTemarioEnDetalle

app_name = 'temario'
urlpatterns = [
    path('temario/', VistaListaTemario.as_view(), name='index'),
    path('temario-en-detalle/<int:pk>', VistaTemarioEnDetalle.as_view(), name='detalle'),
]
