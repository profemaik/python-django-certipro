from django.core.validators import MinValueValidator
from django.db import models
from tinymce import models as tinymce_models


class Tema(models.Model):
    """Almacena el tema a tratar.

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Título del tema.
    """
    titulo = models.CharField('título del tema', max_length=200)
    subtitulo = models.CharField('subtítulo del tema', max_length=250, blank=True)

    class Meta:
        verbose_name = 'Tema'
        verbose_name_plural = 'Temas'
        ordering = ['titulo',]

    def __str__(self):
        return self.titulo


class DetalleTema(models.Model):
    """Almacena el contenido programático y otros aspectos de un tema.

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Título del tema (y edición).
    """
    tema = models.ForeignKey(Tema, on_delete=models.CASCADE)
    horas_academicas = models.SmallIntegerField('horas académicas', validators=[MinValueValidator(8),])
    edicion = models.CharField('edición', max_length=50, help_text='Ej.: 2da. edición')
    contenido = tinymce_models.HTMLField(verbose_name='contenido programático',)

    class Meta:
        verbose_name = 'Detalle del Tema'
        verbose_name_plural = 'Detalles del Tema'
        ordering = ['tema',]

    def __str__(self):
        return f'{self.tema} ({self.edicion})'
