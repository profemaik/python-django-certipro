from django.contrib import admin

from .models import DetalleTema, Tema


class DetalleTemaLineal(admin.StackedInline):
    """Administrador del detalle del tema, mostrado en el administrador
    del tema.

    Args:
        StackedInline (class): Modelo admin Django.
    """
    model = DetalleTema
    extra = 1
    fields = ('edicion', 'horas_academicas', 'contenido')


class TemaAdmin(admin.ModelAdmin):
    """Administrador de los temas y sus contenidos.

    Args:
        ModelAdmin (class): Modelo admin base Django.
    """
    def cantidad_ediciones(self):
        return DetalleTema.objects.filter(tema__pk__exact=self.pk).count()
    cantidad_ediciones.short_description = 'Cantidad de Ediciones'

    fieldsets = [
        ('TEMA', {'fields': ['titulo', 'subtitulo']}),
    ]
    inlines = [DetalleTemaLineal]
    list_display = ('titulo', cantidad_ediciones)
    search_fields = ['titulo']
    list_per_page = 10

admin.site.register(Tema, TemaAdmin)
