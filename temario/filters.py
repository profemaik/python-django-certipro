import django_filters
from django.db import models
from django.forms.widgets import TextInput

from .models import DetalleTema, Tema


class FiltroTemario(django_filters.FilterSet):
    """Filtros para las consultas de los temas y sus detalles registrados.

    Arguments:
        FilterSet {class} -- django-filter
    """
    titulo = django_filters.CharFilter(
        field_name='titulo',
        widget=TextInput(attrs={'placeholder': 'Título'}),
        lookup_expr='icontains',
    )

    class Meta:
        model = Tema
        fields = ['titulo']
