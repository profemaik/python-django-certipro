from django.db import models


class TipoEvento(models.Model):
    """Almacena el tipo de evento (curso, taller, charla, conferencia, etc).

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Nombre del tipo de evento.
    """
    tipoevento_nombre = models.CharField('nombre del tipo de evento', max_length=50)

    class Meta:
        verbose_name = 'Tipo de Evento'
        verbose_name_plural = 'Tipos de Eventos'
        ordering = ['tipoevento_nombre',]

    def __str__(self):
        return self.tipoevento_nombre


class Evento(models.Model):
    """Almacena un evento en la agenda.

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Tipo de evento y tema tratado.
    """
    asistentes = models.ManyToManyField('asistentes.Asistente')
    tema_tratado = models.ForeignKey('temario.DetalleTema', on_delete=models.CASCADE)
    tipo_evento = models.ForeignKey(TipoEvento, on_delete=models.CASCADE, verbose_name='tipo de evento')
    auditorio = models.ForeignKey('auditorios.Auditorio', on_delete=models.CASCADE)
    fecha_inicio = models.DateField('empieza el', auto_now=False, auto_now_add=False)
    fecha_fin = models.DateField('culmina el', auto_now=False, auto_now_add=False)

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'
        ordering = ['tipo_evento', 'tema_tratado']

    def __str__(self):
        return f'{self.tipo_evento}: {self.tema_tratado}'
