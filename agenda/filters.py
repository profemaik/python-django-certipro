import django_filters
from django.db import models
from django.forms.widgets import TextInput

from .models import Evento


class FiltroEvento(django_filters.FilterSet):
    """Filtros para las consultas de los eventos agendados.

    Arguments:
        FilterSet {class} -- django-filter
    """
    tema_tratado = django_filters.CharFilter(
        field_name='tema_tratado',
        widget=TextInput(attrs={'placeholder': 'Tema'}),
        lookup_expr='icontains',
    )
    tipo_evento = django_filters.CharFilter(
        field_name='tipo_evento',
        widget=TextInput(attrs={'placeholder': 'Tipo de Evento'}),
        lookup_expr='icontains',
    )
    auditorio = django_filters.CharFilter(
        field_name='auditorio',
        widget=TextInput(attrs={'placeholder': 'Auditorio'}),
        lookup_expr='icontains',
    )

    class Meta:
        model = Evento
        fields = ['tema_tratado', 'tipo_evento', 'auditorio']
