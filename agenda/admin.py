from django.contrib import admin

from .models import Evento, TipoEvento


@admin.register(TipoEvento)
class TipoEventoAdmin(admin.ModelAdmin):
    """Administrador de los tipos de eventos.

    Args:
        ModelAdmin (class): Modelo admin base de Django.
    """
    pass


@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
    """Administrador de la agenda de eventos.

    Args:
        ModelAdmin (class): Modelo admin base de Django.
    """
    filter_horizontal = ('asistentes',)
    list_display = ('tipo_evento', 'tema_tratado', 'fecha_inicio', 'fecha_fin')
    list_filter = ('fecha_inicio', 'fecha_fin')
    search_fields = ['tipo_evento', 'tema_tratado']
    list_per_page = 10
