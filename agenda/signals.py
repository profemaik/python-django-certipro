from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from asistentes.models import Asistente
from certificados.models import Certificado, PlantillaCertificado

from .models import Evento


@receiver(m2m_changed, sender=Evento.asistentes.through)
def generar_certificado(sender, instance, action, pk_set, **kwargs):
    """Genera el registro del certificado automáticamente cuando un asistente
    es asociado a un evento en particular.

    Args:
        sender (class): Clase intermedia entre Asistente y Evento.
        instance (object): Instancia de la clase Evento.
        action (string): Tipo de acción ejecutada.
        pk_set (set): Conjunto de llaves primarias generadas.

    Returns:
        bool: False si no hay plantilla activa o si no existe el asistente.
    """
    try:
        plantilla_activa = PlantillaCertificado.objects.get(esta_activado=True)
    except PlantillaCertificado.DoesNotExist:
        return False
    except PlantillaCertificado.MultipleObjectsReturned:
        return False

    if action == 'post_add':
        for pk in pk_set:
            try:
                asistente = Asistente.objects.get(pk=pk)
            except Asistente.DoesNotExist:
                return False

            nuevo_certificado = Certificado(plantilla=plantilla_activa, asistente=asistente, evento=instance)
            nuevo_certificado.save()
    elif action == 'post_remove':
        for pk in pk_set:
            try:
                asistente = Asistente.objects.get(pk=pk)
            except Asistente.DoesNotExist:
                return False

            viejo_certificado = Certificado(plantilla=plantilla_activa, asistente=asistente, evento=instance)
            viejo_certificado.delete()
