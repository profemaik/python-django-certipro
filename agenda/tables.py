import django_tables2 as tables

from .models import Evento


class TablaEvento(tables.Table):
    """Permite mostrar los registros de los eventos tabulados en HTML5.

    Arguments:
        Table {class} -- Representación de una tabla según django-tables2.
    """
    acciones = tables.TemplateColumn(template_name='agenda/botones_acciones.html', orderable=False)

    class Meta:
        model = Evento
        template_name = "django_tables2/bootstrap4.html"
        fields = ('tipo_evento', 'tema_tratado',)
