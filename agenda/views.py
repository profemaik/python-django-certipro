from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin

from .filters import FiltroEvento
from .models import Evento, TipoEvento
from .tables import TablaEvento


class VistaListaEvento(LoginRequiredMixin, SingleTableMixin, FilterView):
    """Renderiza la agenda de los eventos registrados en el sistema,
    en formato de tabla.

    Args:
        LoginRequiredMixin (class): Verifica que el usuario esté autentificado.
        SingleTableMixin (class): Agrega una tabla HTMl al contexto.
        FilterView (class): Renderiza los filtros de búsqueda.

    Returns:
        string: HTML de la vista.
    """
    model = Evento
    template_name = 'agenda/lista-agenda.html'
    table_class = TablaEvento
    paginate_by = 10
    filterset_class = FiltroEvento

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site_title'] = 'CertiPro | Eventos'
        context['page_title'] = 'Eventos'
        context['content_title'] = 'Eventos registrados'
        context['url_reporte'] = 'tablero:index'
        context['url_admin'] = 'admin:agenda_evento_changelist'
        return context


class VistaEventoEnDetalle(LoginRequiredMixin, DetailView):
    """Renderiza los detalles de un evento de la agenda en un modal bootstrap.

    Args:
        LoginRequiredMixin (class): Verifica que el usuario esté autentificado.
        DetailView (class): Vista genérica detallada de Django.
    """
    model = Evento
    template_name = 'agenda/detalle-agenda.html'


class VistaReporteGeneralEvento(DetailView):
    pass
