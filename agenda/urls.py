from django.urls import path

from .views import (VistaEventoEnDetalle, VistaListaEvento, VistaReporteGeneralEvento)

app_name = 'agenda'
urlpatterns = [
    path('agenda/', VistaListaEvento.as_view(), name='index'),
    path('agenda-en-detalle/<int:pk>', VistaEventoEnDetalle.as_view(), name='detalle'),
    path('reporte-general-agenda/', VistaReporteGeneralEvento.as_view(), name='reporte-general'),
]
