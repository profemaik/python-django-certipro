from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import DetailView
from django_filters.views import FilterView
from django_tables2.paginators import LazyPaginator
from django_tables2.views import SingleTableMixin
from wkhtmltopdf.views import PDFTemplateResponse

from agenda.models import Evento

from .filters import FiltroCertificado
from .models import Certificado, PlantillaCertificado
from .tables import TablaCertificados


def exists_default_certificate_template():
    """Consulta en la BD si existe una plantilla de certificado activa.

    Args:
        request (object): Instancia de HttpRequest.

    Returns:
        object|bool: Instancia de PlantillaCertificado o False si no existe.
    """
    try:
        default_template = PlantillaCertificado.objects.get(esta_activado=True)
    except PlantillaCertificado.DoesNotExist:
        return False
    except PlantillaCertificado.MultipleObjectsReturned:
        return False

    return default_template


def create_pending_certificates(certificate_template):
    """Genera los certificados que faltan siempre y cuando exista una plantilla
    de certificado activada.

    Args:
        request (object): Instancia de HttpRequest.
        certificate_template (object): Instancia de PlantillaCertificado.
    """
    # TODO Verificar la cantidad de certificados por generar.
    if not certificate_template:
        return

    didactic_events = Evento.objects.all()
    if not didactic_events:
        return

    for event in didactic_events:
        try:
            Certificado.objects.get(evento=event.pk)
        except Certificado.DoesNotExist:
            for attendee in event.asistentes.all():
                Certificado.objects.create(plantilla=certificate_template, evento=event, asistente=attendee)
        except Certificado.MultipleObjectsReturned:
            return


class VistaListaCertificados(LoginRequiredMixin, SingleTableMixin, FilterView):
    """Renderiza los certificados en una tabla HTML5.

    Args:
        LoginRequiredMixin (class): Verifica si el usuario está autentificado.
        SingleTableMixin (class): Agrega un objeto Tabla al contexto.
        FilterView (class): Renderiza los filtros de búsqueda.

    Returns:
        dict: Datos del contexto.
    """
    model = Certificado
    template_name = 'certificados/lista-certificados.html'
    table_class = TablaCertificados
    paginate_by = 5
    paginator_class = LazyPaginator
    filterset_class = FiltroCertificado
    notify_alert = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['notify_alert'] = self.notify_alert
        context['site_title'] = 'CertiPro | Certificados'
        context['page_title'] = 'Certificados'
        context['content_title'] = 'Certificados registrados'
        context['url_reporte'] = 'certificados:reporte-general'
        context['url_admin'] = 'admin:certificados_plantillacertificado_changelist'
        return context

    def dispatch(self, request, *args, **kwargs):
        certificate_template = exists_default_certificate_template()
        if certificate_template:
            create_pending_certificates(certificate_template)
        else:
            self.notify_alert = {
                'strong': 'No se ha definido una plantilla de certificado.',
                'text': 'Mientras no exista una plantilla de certificado activa, \
                        el sistema no podrá generar certificados.'
            }
        return super(VistaListaCertificados, self).dispatch(request, *args, **kwargs)


class VistaCertificadoEnDetalle(LoginRequiredMixin, DetailView):
    """Renderiza el registro detallado de un certificado particular.

    Arguments:
        DetailView {class} -- Vista detallada genérica de Django.
    """
    model = Certificado
    template_name = 'certificados/detalle-certificado.html'


class VistaReporteCertificadoAsistente(LoginRequiredMixin, DetailView):
    """Aquí no hay nada que hacer, por ahora.

    Args:
        LoginRequiredMixin (class): Verifica que el usuario está autentificado.
        DetailView (class): Renderiza una vista detallada de un objeto dado.
    """
    pass


@xframe_options_exempt
def reporte_certificado(request, certf_id):
    """Genera el archivo PDF del certificado usando wkhtmltopdf.

    Args:
        request (object): Instancia de HttpRequest.
        certf_id (string): UUID del certificado.

    Returns:
        class: PDFTemplateResponse.
    """
    certificado = get_object_or_404(Certificado, pk=certf_id)
    contexto = {
        'nombre_asistente': certificado.asistente.nombres,
        'apellido_asistente': certificado.asistente.apellidos,
        'ci_asistente': f'{certificado.asistente.nacionalidad} {certificado.asistente.numero_ci}',
        'tipo_evento': certificado.evento.tipo_evento,
        'nombre_evento': certificado.evento.tema_tratado,
        'horas_evento': certificado.evento.tema_tratado.horas_academicas,
        'fecha_inicio_evento': certificado.evento.fecha_fin,
        'fecha_fin_evento': certificado.evento.fecha_fin,
        'nombre_auditorio': certificado.evento.auditorio,
        'ciudad_auditorio': certificado.evento.auditorio.localidad,
        'url_fondo': certificado.plantilla.fondo_frontal,
    }
    config_pdf = {
        'margin-bottom': certificado.plantilla.margen_inf,
        'margin-left': certificado.plantilla.margen_izq,
        'margin-right': certificado.plantilla.margen_der,
        'margin-top': certificado.plantilla.margen_sup,
        'page-size': certificado.plantilla.formato_hoja,
        'orientation': 'Landscape'
    }

    return PDFTemplateResponse(
        request=request,
        context=contexto,
        template='certificados/certificado.html',
        filename=None,
        cmd_options=config_pdf
    )
