import django_filters
from django.forms.widgets import DateInput, NumberInput, Select, TextInput

from agenda.models import TipoEvento

from .models import Certificado


class FiltroCertificado(django_filters.FilterSet):
    """Filtros para las consultas de los certificados registrados.

    Arguments:
        FilterSet (class) -- django_filters
    """
    numero_ci_asistente = django_filters.NumberFilter(
        field_name='asistente__numero_ci',
        widget=NumberInput(attrs={'placeholder': 'CI del asistente'}),
        lookup_expr='icontains',
    )
    nombres_asistente = django_filters.CharFilter(
        field_name='asistente__nombres',
        widget=TextInput(attrs={'placeholder': 'Nombre(s) del asistente'}),
        lookup_expr='icontains',
    )
    apellidos_asistente = django_filters.CharFilter(
        field_name='asistente__apellidos',
        widget=TextInput(attrs={'placeholder': 'Apellido(s) del asistente'}),
        lookup_expr='icontains',
    )
    evento = django_filters.CharFilter(
        field_name='evento__tema_tratado__titulo',
        widget=TextInput(attrs={'placeholder': 'Tema'}),
        lookup_expr='icontains',
    )
    tipo_evento = django_filters.ModelChoiceFilter(
        queryset=TipoEvento.objects.all(),
        field_name='evento__tipo_evento__tipoevento_nombre',
        widget=Select(attrs={'placeholder': 'Tipo de Evento'}),
    )
    fecha_inicio = django_filters.DateFilter(
        field_name='evento__fecha_inicio',
        widget=DateInput(attrs={'type': 'date'}),
    )
    fecha_fin = django_filters.DateFilter(
        field_name='evento__fecha_fin',
        widget=DateInput(attrs={'type': 'date'}),
    )

    class Meta:
        model = Certificado
        fields = [
            'numero_ci_asistente', 'nombres_asistente', 'apellidos_asistente',
            'evento', 'tipo_evento', 'fecha_inicio', 'fecha_fin'
        ]
