"""Funciones que ayudan en tareas relacionadas a la gestión de certificados.
"""
import os


def ruta_fondo_frontal(instance, filename):
    """Define el nombre y la ruta del archivo a partir de: título dado a la
    plantilla y el nombre original del archivo adjuntado. `FileSystemStorage`
    se encarga de limpiar la cadena devuelta.

    Args:
        instance (object): Instancia particular donde la imagen está siendo
                           adjuntada.
        filename (string): El nombre original del archivo.

    Returns:
        string: 'certificado/fondo_f_título plantilla'.
    """
    file_ext = os.path.splitext(filename)[1]
    return 'certificado/fondo_f_{0}{1}'.format(instance.titulo.lower(), file_ext.lower())


def ruta_fondo_trasero(instance, filename):
    """Define el nombre y la ruta del archivo a partir de: título dado a la
    plantilla y el nombre original del archivo adjuntado. `FileSystemStorage`
    se encarga de limpiar la cadena devuelta.

    Args:
        instance (object): Instancia particular donde la imagen está siendo
                           adjuntada.
        filename (string): El nombre original del archivo.

    Returns:
        string: 'certificado/fondo_t_título plantilla'.
    """
    file_ext = os.path.splitext(filename)[1]
    return 'certificado/fondo_t_{0}{1}'.format(instance.titulo.lower(), file_ext.lower())
