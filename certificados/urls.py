from django.urls import path

from .views import (
    VistaCertificadoEnDetalle, VistaListaCertificados,
    VistaReporteCertificadoAsistente, reporte_certificado
)

app_name = 'certificados'
urlpatterns = [
    path('certificados/', VistaListaCertificados.as_view(), name='index'),
    path(
        'certificado-en-detalle/<uuid:pk>',
        VistaCertificadoEnDetalle.as_view(),
        name='detalle'
    ),
    path(
        'reporte-certificado/<uuid:certf_id>',
        reporte_certificado,
        name='reporte-asistente'
    ),
    path(
        'reporte-certificado/',
        VistaReporteCertificadoAsistente.as_view(),
        name='reporte-general'
    ),
]
