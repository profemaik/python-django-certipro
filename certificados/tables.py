import django_tables2 as tables

from .models import Certificado


class TablaCertificados(tables.Table):
    """Datos tabulados de los certificados registrados.

    Arguments:
        tables (class) -- django-tables2
    """
    acciones = tables.TemplateColumn(template_name='certificados/botones_acciones.html', orderable=False)
    asistente__nombres = tables.Column(verbose_name='Nombre(s) del asistente')
    asistente__apellidos = tables.Column(verbose_name='Apellido(s) del asistente')
    evento__tema_tratado = tables.Column(verbose_name='Tema')

    class Meta:
        model = Certificado
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'asistente__nombres', 'asistente__apellidos',
            'evento__tipo_evento', 'evento__tema_tratado',
            'evento__fecha_inicio', 'evento__fecha_fin')
