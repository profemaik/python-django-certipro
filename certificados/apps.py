from django.apps import AppConfig


class CertificadosConfig(AppConfig):
    name = 'certificados'

    def ready(self):
        import certificados.signals
