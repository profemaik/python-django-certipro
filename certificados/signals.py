import os

from django.db.models.signals import post_delete, pre_save, post_save
from django.dispatch import receiver

from .models import PlantillaCertificado


INI_HTML_CERTIFICADO = [
    "<!DOCTYPE html>\n",
    "<html>\n",
    "<head>\n",
    "  <meta charset='UTF-8'>\n",
    "  <title>certificado</title>\n",
    "  <style>\n",
    "    .plantilla-certificado {\n",
      "     background-image: url('http://127.0.0.1:8000/media/{{ url_fondo }}');\n",
      "     background-color: #cccccc;\n",
      "     background-position: center;\n",
      "     background-repeat: no-repeat;\n",
      "     background-size: cover;\n",
      "     position: relative;\n",
    "    }\n",
    "  </style>\n",
    "</head>\n",
    "<body>\n",
    "  <div class='plantilla-certificado'>\n",
    "    <div class='texto-certificado'>\n"
]
FIN_HTML_CERTIFICADO = [
    "    </div>\n",
    "  </div>\n",
    "</body>\n",
    "</html>\n"
]


@receiver(post_delete, sender=PlantillaCertificado)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `PlantillaCertificado` object is deleted.

    https://stackoverflow.com/questions/16041232/django-delete-filefield
    """
    if instance.fondo_frontal and instance.fondo_frontal != 'plantillas/certificado/en_blanco.png':
        if os.path.isfile(instance.fondo_frontal.path):
            os.remove(instance.fondo_frontal.path)

    if instance.fondo_trasero and instance.fondo_trasero != 'plantillas/certificado/en_blanco.png':
        if os.path.isfile(instance.fondo_trasero.path):
            os.remove(instance.fondo_trasero.path)


@receiver(pre_save, sender=PlantillaCertificado)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `PlantillaCertificado` object is updated
    with new file.

    https://stackoverflow.com/questions/16041232/django-delete-filefield
    """
    if not instance.pk:
        return False

    try:
        old_file = PlantillaCertificado.objects.get(pk=instance.pk).fondo_frontal
    except PlantillaCertificado.DoesNotExist:
        return False

    new_file = instance.fondo_frontal
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)

    try:
        old_file = PlantillaCertificado.objects.get(pk=instance.pk).fondo_trasero
    except PlantillaCertificado.DoesNotExist:
        return False

    new_file = instance.fondo_trasero
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@receiver(post_save, sender=PlantillaCertificado)
def crea_archivo_plantilla_html(sender, instance, **kwargs):
    """Crea el archivo html, que servirá como plantilla para generar el pdf del
    certificado, en la carpeta templates de la app.

    Args:
        sender (class): PlantillaCertificado.
        instance (object): Instancia de PlantillaCertificado.

    Returns:
        bool: False si no se creó un nuevo registro.
    """
    if not instance.esta_activado:
        return False

    with open('certificados\\templates\\certificados\\certificado.html', 'w') as archivo_html:
        archivo_html.writelines(INI_HTML_CERTIFICADO)
        archivo_html.writelines(instance.txt_frontal)
        archivo_html.writelines(FIN_HTML_CERTIFICADO)
        archivo_html.write("\n")


@receiver(post_save, sender=PlantillaCertificado)
def auto_activa_plantilla(sender, instance, created, **kwargs):
    """Mantiene como activa una sola plantilla de certificado en operaciones
    de tipo `INSERT` y `UPDATE`.

    Args:
        sender (class): PlantillaCertificado.
        instance (object): Instancia actual de PlantillaCertificado.

    Returns:
        bool: False si no hay nada que hacer.
    """
    if not instance.pk:
        return False

    if instance.esta_activado:
        try:
            PlantillaCertificado.objects.exclude(pk=instance.pk).update(esta_activado=False)
        except PlantillaCertificado.DoesNotExist:
            return False
    else:
        try:
            PlantillaCertificado.objects.exclude(pk=instance.pk).get(esta_activado=True)
        except PlantillaCertificado.DoesNotExist:
            instance.esta_activado = True
        except PlantillaCertificado.MultipleObjectsReturned:
            instance.esta_activado = True
            PlantillaCertificado.objects.exclude(pk=instance.pk).update(esta_activado=False)
