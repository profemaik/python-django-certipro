from django.contrib import admin

from .models import PlantillaCertificado


@admin.register(PlantillaCertificado)
class PlantillaCertificadoAdmin(admin.ModelAdmin):
    """Administrador de las `plantillas` para los certificados.

    Args:
        admin (class): Modelo admin base de Django.
    """
    pass
