import uuid

from django.db import models
from tinymce import models as tinymce_models

from .helpers import ruta_fondo_frontal, ruta_fondo_trasero


class PlantillaCertificado(models.Model):
    """Almacena la estructura base de los certificados.

    Args:
        models (class): Modelo base de Django.

    Returns:
        string: Nombre único identificativo de la plantilla.
    """
    FORMATO_CARTA = 'letter'
    FORMATO_OFICIO = 'legal'
    FORMATO_A4 = 'A4'
    LISTA_FORMATOS = [
        (FORMATO_A4, 'A4'),
        (FORMATO_CARTA, 'Carta'),
        (FORMATO_OFICIO, 'Oficio'),
    ]
    titulo = models.CharField('nombre', max_length=50, unique=True)
    fondo_frontal = models.ImageField(
        'marco frontal',
        upload_to=ruta_fondo_frontal,
        default='plantillas/certificado/en_blanco.png',
        help_text='Imagen usada como plantilla en la cara frontal.'
    )
    fondo_trasero = models.ImageField(
        'marco trasero',
        upload_to=ruta_fondo_trasero,
        default='plantillas/certificado/en_blanco.png',
        help_text='Imagen usada como plantilla en la cara posterior.'
    )
    txt_frontal = tinymce_models.HTMLField(verbose_name='texto frontal',)
    formato_hoja = models.CharField('tamaño', max_length=32, choices=LISTA_FORMATOS)
    margen_izq = models.PositiveSmallIntegerField('margen izquierdo')
    margen_der = models.PositiveSmallIntegerField('margen derecho')
    margen_sup = models.PositiveSmallIntegerField('margen superior')
    margen_inf = models.PositiveSmallIntegerField('margen inferior')
    contenido_esimprimible = models.BooleanField('imprimir el temario en cara posterior', default=True,)
    esta_activado = models.BooleanField('predeterminar plantilla', default=True,)

    class Meta:
        verbose_name = 'Plantilla'
        verbose_name_plural = 'Plantillas'
        ordering = ['titulo']

    def __str__(self):
        return self.titulo


class Certificado(models.Model):
    """Almacena los certificados de los asistentes a los eventos.

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Identificador único universal del certificado.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    plantilla = models.ForeignKey(
        PlantillaCertificado, on_delete=models.CASCADE, verbose_name='plantilla en uso'
    )
    asistente = models.ForeignKey(
        'asistentes.Asistente', on_delete=models.CASCADE, verbose_name='asistente al evento'
    )
    evento = models.ForeignKey(
        'agenda.Evento', on_delete=models.CASCADE, verbose_name='evento'
    )

    class Meta:
        verbose_name = 'Certificado'
        verbose_name_plural = 'Certificados'
        ordering = ['asistente']

    def __str__(self):
        return str(self.id)
