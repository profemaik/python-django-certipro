from django.apps import AppConfig


class AuditoriosConfig(AppConfig):
    name = 'auditorios'
