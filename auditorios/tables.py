import django_tables2 as tables

from .models import Auditorio


class TablaAuditorios(tables.Table):
    """Datos tabulados de los auditorios registrados.

    Arguments:
        Table {class} -- Representación de una tabla según django-tables2.
    """
    acciones = tables.TemplateColumn(template_name='auditorios/botones_acciones.html', orderable=False)

    class Meta:
        model = Auditorio
        template_name = "django_tables2/bootstrap4.html"
        fields = ('nombre', 'localidad',)
        sequence = ('nombre', 'localidad',)
