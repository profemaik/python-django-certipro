from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin

from .filters import FiltroAuditorio
from .models import Auditorio
from .tables import TablaAuditorios


class VistaListaAuditorios(LoginRequiredMixin, SingleTableMixin, FilterView):
    """Renderiza el catálogo/listado de auditorios registrados en el sistema,
    en formato de tabla.

    Arguments:
        ListView {class} -- Vista de lista genérica de Django.
    """
    model = Auditorio
    template_name = 'auditorios/lista-auditorios.html'
    table_class = TablaAuditorios
    paginate_by = 10
    filterset_class = FiltroAuditorio

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site_title'] = 'CertiPro | Auditorios'
        context['page_title'] = 'Auditorios'
        context['content_title'] = 'Auditorios registrados'
        context['url_reporte'] = 'tablero:index'
        context['url_admin'] = 'admin:auditorios_auditorio_changelist'
        return context


class VistaAuditorioEnDetalle(LoginRequiredMixin, DetailView):
    model = Auditorio
    template_name = 'auditorios/detalle-auditorio.html'
