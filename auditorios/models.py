from django.db import models


class Auditorio(models.Model):
    """Almacena los datos de un auditorio.

    Args:
        Model (class): Modelo base de Django.

    Returns:
        string: Nombre del auditorio.
    """
    localidad = models.CharField('localidad/Ciudad', max_length=200, help_text='Ej.: Caracas')
    nombre = models.CharField('nombre', max_length=200, help_text='Ej.: Sala Ríos Reina del Teatro Teresa Carreño')

    class Meta:
        verbose_name = 'Auditorio'
        verbose_name_plural = 'Auditorios'
        ordering = ['nombre',]

    def __str__(self):
        return self.nombre
