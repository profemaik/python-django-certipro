from django.contrib import admin

from .models import Auditorio


@admin.register(Auditorio)
class AuditorioAdmin(admin.ModelAdmin):
    """Administrador de los auditorios.

    Args:
        ModelAdmin (class): Modelo admin base de Django.
    """
    list_display = ('nombre', 'localidad',)
    list_filter = ('localidad',)
    search_fields = ('nombre',)
