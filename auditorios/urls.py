from django.urls import path

from .views import VistaAuditorioEnDetalle, VistaListaAuditorios

app_name = 'auditorios'
urlpatterns = [
    path('auditorios/', VistaListaAuditorios.as_view(), name='index'),
    path('auditorio-en-detalle/<int:pk>', VistaAuditorioEnDetalle.as_view(), name='detalle'),
]
