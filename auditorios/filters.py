import django_filters
from django.db import models
from django.forms.widgets import TextInput

from .models import Auditorio


class FiltroAuditorio(django_filters.FilterSet):
    """Filtros para las consultas de los auditorios registrados.

    Arguments:
        FilterSet {class} -- django-filter
    """
    nombre = django_filters.CharFilter(
        field_name='nombre',
        widget=TextInput(attrs={'placeholder': 'Nombre'}),
        lookup_expr='icontains',
    )
    localidad = django_filters.CharFilter(
        field_name='localidad',
        widget=TextInput(attrs={'placeholder': 'Localidad/Ciudad'}),
        lookup_expr='icontains',
    )

    class Meta:
        model = Auditorio
        fields = ['nombre', 'localidad']
