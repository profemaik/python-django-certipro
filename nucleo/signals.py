import os

from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch import receiver


@receiver(pre_save, sender=settings.AUTH_USER_MODEL)
def auto_elimina_imagen_de_perfil(sender, instance, **kwargs):
    """Elimina la imagen de perfil del usuario automáticamente.

    Args:
        sender (class): User.
        instance (object): Instancia actual de User.

    Returns:
        bool: False, si no hay nada que hacer.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).avatar
    except sender.DoesNotExist:
        return False

    if old_file != 'avatares/undraw_profile_pic_ic5t.svg':
        new_file = instance.avatar
        if not (old_file == new_file) and old_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
