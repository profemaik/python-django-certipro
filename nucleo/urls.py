from django.urls import path
from .views import ModificacionDePerfilDeUsuario

app_name = 'nucleo'
urlpatterns = [
    path('usuario/mi-perfil/<int:pk>', ModificacionDePerfilDeUsuario.as_view(), name='perfilusuario')
]