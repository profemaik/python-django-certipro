from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User

class CustomUserCreationForm(UserCreationForm):
    """Formulario personalizado para la creación de nuevos usuarios en el
    sistema.

    Args:
        UserCreationForm (class): Formulario para crear usuarios sin
        privilegios de Django.
    """

    class Meta:
        model = User
        fields = ('username', 'email', 'avatar')

class CustomUserChangeForm(UserChangeForm):
    """Formulario personalizado para la modificación de usuarios existentes
    en el sistema.

    Args:
        UserChangeForm (class): Formulario para editar usuarios de Django.
    """

    class Meta:
        model = User
        fields = ('username', 'email', 'avatar')


class ProfileUserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'avatar')
