from django.contrib.auth import get_user_model
#from django.conf import settings
#from django.shortcuts import render
from django.views.generic import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import User


class ModificacionDePerfilDeUsuario(LoginRequiredMixin, UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'avatar']
    template_name = "nucleo/perfildeusuario_modif.html"
    #success_url = '/'
    success_url = reverse_lazy('tablero:index')

    def get_object(self):
        return User.objects.get(username=self.request.user)
