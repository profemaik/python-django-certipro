"""Constantes utilizadas en otras apps o módulos del sistema.
"""


MODULOS_USUARIO = {
    'certificado': {
        'nombre': 'Certificados',
        'nombre_app': 'certificados',
        'icono': 'fas fa-fw fa-certificate',
        'url': 'index',
        'estatus': '',
    },
    'asistente': {
        'nombre': 'Asistentes',
        'nombre_app': 'asistentes',
        'icono': 'fas fa-fw fa-users',
        'url': 'lista-asistentes',
        'estatus': '',
    },
    'evento': {
        'nombre': 'Eventos',
        'nombre_app': 'evento',
        'icono': 'fas fa-fw fa-calendar-alt',
        'url': 'index',
        'estatus': '',
    },
    'temario': {
        'nombre': 'Temario',
        'nombre_app': 'temario',
        'icono': 'fas fa-fw fa-clipboard-list',
        'url': 'index',
        'estatus': '',
    },
    'auditorio': {
        'nombre': 'Auditorios',
        'nombre_app': 'auditorios',
        'icono': 'fas fa-fw fa-university',
        'url': 'lista-auditorios',
        'estatus': '',
    },
}

MENU_SISTEMA = {
    'Favoritos': {
        'Certificados': {
            'opciones': {
                'icono': 'fas fa-fw fa-certificate',
                'url': 'lista-certificados',
                'estatus': '',
            },
        },
        'Asistentes': {
            'opciones': {
                'icono': 'fas fa-fw fa-users',
                'url': 'lista-asistentes',
                'estatus': '',
            },
        },
        'Agenda': {
            'opciones': {
                'icono': 'fas fa-fw fa-calendar-check',
                'url': 'lista-agenda',
                'estatus': '',
            },
        },
    },
    'Maestros': {
        'Temario': {
            'opciones': {
                'icono': 'fas fa-fw fa-clipboard-list',
                'url': 'lista-temario',
                'estatus': '',
            },
        },
        'Auditorios': {
            'opciones': {
                'icono': 'fas fa-fw fa-university',
                'url': 'lista-auditorios',
                'estatus': '',
            },
        },
    },
    'Sistema': {
        'Usuarios y Grupos': {
            'opciones': {
                'id': 'usuGrupos',
                'encabezado': '', # <h6 class="collapse-header">Custom Components:</h6>
                'icono': 'fas fa-fw fa-users',
                'estatus': '',
                'expandido': 'true',
                'plegado': 'collapsed',
                'mostrar': '',
            },
            'submenu': {
                'Grupos': {
                    'url': 'index',
                    'estatus': '',
                },
                'Usuarios': {
                    'url': 'index',
                    'estatus': '',
                },
            },
        },
    },
}
