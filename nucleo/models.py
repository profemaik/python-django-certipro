from django.contrib.auth.models import AbstractUser
from django.db import models

from .validators import valida_peso_imagen


class User(AbstractUser):
    """Modelo de usuario personalizable.

    Args:
        AbstractUser (class): Implementa todas las características de un
        modelo de usuario compatible con permisos de admin.
    """
    avatar = models.ImageField(
        verbose_name='mi imagen', upload_to='avatares/usuarios', blank=True,
        default='avatares/undraw_profile_pic_ic5t.svg', validators=[valida_peso_imagen]
    )

    def __str__(self):
        return self.username
