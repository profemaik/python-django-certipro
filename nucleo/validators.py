from django.core.exceptions import ValidationError


def valida_peso_imagen(valor):
    """Verifica el peso, en Kilobytes, de la imagen sabiendo que 1 KB = 1024 B.

    Args:
        valor (int): Tamaño total de la imagen en bytes.

    Raises:
        ValidationError: Cuando el tamaño de la imagen sea superior a 50 KB.
    """
    if valor.size > 50 * 1024:
        raise ValidationError("El tamaño máximo permitido de la imagen es de 50 Kilobytes")

