"""Funciones ayudantes compartidas entre las aplicaciona o módulos del
sistema.
"""
from .constants import MENU_SISTEMA, MODULOS_USUARIO


def activaEstatusModulo(modulo_usuario=None):
    """Establece el valor del índice 'estatus' en 'active' del diccionario
    MODULOS_USUARIOS.

    Args:
        modulo_usuario (string, optional): Nombre del módulo en minúsculas
        y en singular. Defaults to None.

    Returns:
        dict: MODULOS_USUARIO
    """
    modulos = MODULOS_USUARIO
    for mod in list(modulos.keys()):
        if mod == modulo_usuario:
            modulos[mod]['estatus'] = 'active'
        else:
            modulos[mod]['estatus'] = ''
    return modulos


def activaMenuSimple(elemento=None):
    #menu = MENU_SISTEMA
    for nombre_grupo,grupos in MENU_SISTEMA.items():
        for nombre_item, opciones in grupos.items():
            if nombre_item == elemento:
                opciones['opciones']['estatus'] = 'active'
            else:
                opciones['opciones']['estatus'] = ''
    return MENU_SISTEMA


def activaMenuCompuesto(contenedor=None, elemento=None):
    for nombre_grupo,grupos in MENU_SISTEMA.items():
        for nombre_contenedor,contenidos in grupos.items():
            if nombre_contenedor == contenedor:
                contenidos['opciones']['estatus'] = 'active'
                contenidos['opciones']['expandido'] = 'true'
                contenidos['opciones']['mostrar'] = 'collapse show'
                for nombre_submenu,opciones in contenidos['submenu'].items():
                    if nombre_submenu == elemento:
                        opciones['estatus'] = 'active'
                    else:
                        opciones['estatus'] = ''
            else:
                contenidos['opciones']['estatus'] = ''
                contenidos['opciones']['expandido'] = 'false'
                contenidos['opciones']['mostrar'] = ''
            #    for nombre_submenu,opciones in contenidos.items():
            #        opciones['estatus'] = ''
    return MENU_SISTEMA
