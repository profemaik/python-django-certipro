from django.contrib.admin.apps import AdminConfig


class CertiproAdminConfig(AdminConfig):
    default_site = 'certipro.admin.CertiproAdminSite'
