from django.contrib import admin


class CertiproAdminSite(admin.AdminSite):
    site_title = 'CertiPro: Administrador'
    site_header = 'CertiPro'
    index_title = 'Sitio Administrador CertiPro'
