"""certipro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static

urlpatterns = [
    path('', include('tablero.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('asistentes/', include('asistentes.urls')),
    path('auditorios/', include('auditorios.urls')),
    path('temario/', include('temario.urls')),
    path('agenda/', include('agenda.urls')),
    path('certificados/', include('certificados.urls')),
    path('usuario/', include('nucleo.urls')),
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('debugg/', include(debug_toolbar.urls)),
    ] + urlpatterns
